function isEmpty(obj){
  if(obj === null || obj === undefined)
    return true
  return Object.keys(obj).length === 0 && obj.constructor === Object
}

function formattedDate(date, isTime){
    if(isEmpty(date))
      return null

    const day = addZero(date.getDate())
    const month = addZero(date.getMonth() + 1)
    const year = date.getFullYear()

    if(!isTime)
      return `${year}-${month}-${day}`

    const time = date.toTimeString().slice(0,17)

    return `${year}-${month}-${day} ${time}`
}

function addZero(x) {
  if(Number(x) < 10) {
    return `0${x}`
  }
  return x
}

/*
  total: 24
  red accent-4, blue accent-4, green accent-4, yellow accent-4, pink, purple accent-4, indigo, light-blue , cyan accent-2, cyan accent-4,
  cyan, teal, teal accent-2, teal accent-3, teal accent-4, light-green, light-green accent-3, lime accent-3, amber accent-4,
  amber darken-4, orange, brown darken-2, blue-grey, grey, black
*/
const colors = ['#D50000', '#2962FF', '#00C853', '#FFD600', '#E91E63', '#AA00FF', '#3F51B5', '#03A9F4', '#18FFFF',
                '#00B8D4', '#009688', '#64FFDA', '#1DE9B6', '#00BFA5', '#8BC34A', '#76FF03', '#C6FF00', '#FFAB00',
                '#FF6F00', '#FF9800', '#5D4037', '#607D8B', '#9E9E9E', '#000000']

export {isEmpty, formattedDate, colors}
