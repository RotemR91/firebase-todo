import { isEmpty } from '@/assets/utils/utils.js'

const getters = {
  //user
  isAuthenticated: (state) => !!state.user,
  isAnonymous: (state, getters) => getters.isAuthenticated && state.user.isAnonymous,
  //profiles
  profile: (state) => !isEmpty(state.profiles) && !isEmpty(state.user) ? state.profiles[state.user.uid] : null,
  isAdmin: (state, getters) => getters.profile ? getters.profile.isAdmin : null,
  membersNames: (state) => !isEmpty(state.user) && !isEmpty(state.profiles) ? Object.values(state.profiles).map(profile => profile.name) : null,
  //groups
  defaultGroupName: (state, getters) => {
    if(!isEmpty(getters.profile)) {
      if(getters.profile.defaultGroup === 'Private') {
        return 'Private'
      }
      else if(!isEmpty(state.groups)) {
        let group = state.groups[getters.profile.defaultGroup]
        if(group) return group.name
      }
    }
    return null
  },
  group: (state, getters) => state.currentGroupId && state.groups ? state.groups[state.currentGroupId] : null,
  isGroupAdmin: (state, getters) => getters.profile && getters.group ? getters.group.uid === getters.profile.id : null,
  // spaces
  defaultSpace: (state, getters) => !isEmpty(getters.group) ? getters.group.defaultGroupSpace : getters.profile ? getters.profile.defaultSpace : null,
  defaultSpaceName: (state, getters) => {
    if(!isEmpty(getters.allSpaces) && !isEmpty(getters.defaultSpace)){
      let space = getters.allSpaces[getters.defaultSpace]
      if(space) return space.name
    }
    return null
  },
  allSpaces: (state) => {
    if(!isEmpty(state.spaces)) {
      if(!isEmpty(state.groupSpaces)) {
        return {...state.spaces, ...state.groupSpaces}
      }
      return state.spaces
    }
    else if(!isEmpty(state.groupSpaces)) {
      return state.groupSpaces
    }
    return null
  },
  space: (state, getters) => {
    if(state.currentSpaceId === 'All-Group') {
      return 'All-Group'
    }
    if(state.currentSpaceId === 'All-Groups') {
      return 'All-Groups'
    }
    //group
    if(state.currentSpaceId && !isEmpty(getters.allSpaces)) {
      return getters.allSpaces[state.currentSpaceId]
    }
    //private
    else if(getters.profile && getters.defaultSpace && !isEmpty(getters.allSpaces)) {
      if(getters.profile.groups.length === 0) {
        return getters.allSpaces[getters.defaultSpace]
      }
    }
    return null
  },
  activeSpaces: (state, getters) => {
    let spaces = []
    //group
    if(!isEmpty(getters.group) && !isEmpty(state.groupSpaces) && getters.group.spaces) {
      getters.group.spaces.forEach(space => {
        if(state.groupSpaces[space] !== undefined) {
          spaces.push(state.groupSpaces[space])
        }
      })
      return spaces
    }
    //private
    else {
      if(!isEmpty(state.spaces) && getters.profile.spaces)  {
        if(getters.profile.groups.length > 0) {
          getters.profile.spaces.forEach(space => {
            if(state.spaces[space] !== undefined) {
              spaces.push(state.spaces[space])
            }
          })
          return spaces
        }
      }
    }
    return null
  },
  //tasks
  allActiveTasks: (state) => {
    if(!isEmpty(state.activeTasks)) {
      if(!isEmpty(state.activeGroupTasks)) {
        return {...state.activeTasks, ...state.activeGroupTasks}
      }
      return state.activeTasks
    }
    else if(!isEmpty(state.activeGroupTasks)) {
      return state.activeGroupTasks
    }
    return null
  },
  allArchivedTasks: (state) => {
    if(!isEmpty(state.archivedTasks)) {
      if(!isEmpty(state.archivedGroupTasks)) {
        return {...state.archivedTasks, ...state.archivedGroupTasks}
      }
      return state.archivedTasks
    }
    else if(!isEmpty(state.archivedGroupTasks)) {
      return state.archivedGroupTasks
    }
    return null
  },
  allTasks: (state, getters) => {
    if(!isEmpty(getters.allActiveTasks)) {
      if(!isEmpty(getters.allArchivedTasks)) {
        return {...getters.allActiveTasks, ...getters.allArchivedTasks}
      }
      return getters.allActiveTasks
    }
    else if(!isEmpty(getters.allArchivedTasks)) {
      return getters.allArchivedTasks
    }
  },
  activeSpacesTasks: (state, getters) => {
    if(!isEmpty(getters.space)) {
      if(getters.space === 'All-Group') {
        if(!isEmpty(getters.group) && !isEmpty(state.activeGroupTasks)) {
          return Object.values(state.activeGroupTasks).filter(task => task.gid === getters.group.id)
        }
        if(!isEmpty(state.activeTasks)) {
          return Object.values(state.activeTasks)
        }
      }
      else if(getters.space === 'All-Groups' && !isEmpty(getters.allActiveTasks)) {
        return Object.values(getters.allActiveTasks)
      }
      if(!isEmpty(getters.allActiveTasks)) {
        return Object.values(getters.allActiveTasks).filter(task => task.space === getters.space.id)
      }
    }
    return null
  },
  archivedSpacesTasks: (state, getters) => {
    if(!isEmpty(getters.space)) {
      if(getters.space === 'All-Group') {
        if(!isEmpty(getters.group) && !isEmpty(state.archivedGroupTasks)) {
          return Object.values(state.archivedGroupTasks).filter(task => task.gid === getters.group.id)
        }
        if(!isEmpty(state.archivedTasks)) {
          return Object.values(state.archivedTasks)
        }
      }
      else if(getters.space === 'All-Groups' && !isEmpty(getters.allArchivedTasks)) {
        return Object.values(getters.allArchivedTasks)
      }
      if(!isEmpty(getters.allArchivedTasks)) {
        return Object.values(getters.allArchivedTasks).filter(task => task.space === getters.space.id)
      }
    }
    return null
  },
}

export default getters
