import Vue from 'vue'
import initialState from './state.js';

const mutations = {
  //user
  setUser(state, user) {
    const { uid, email, emailVerified } = user
    state.user = { uid, email, emailVerified }
  },
  userReset(state) {
    Object.assign(state, initialState())
  },
  addProfile(state, profile) {
    if(state.profiles === null) state.profiles = profile
    else state.profiles = {...state.profiles, ...profile}
  },
  removeProfile(state, id){
    delete state.profiles[id]
    state.profiles = Object.assign({}, state.profiles)
  },
  //spaces
  addSpace(state, space){
    if(state.spaces === null) state.spaces = space
    else state.spaces = {...state.spaces, ...space}
  },
  removeSpace(state, id){
    delete state.spaces[id]
    state.spaces = Object.assign({}, state.spaces)
  },
  //groups
  addGroup(state, group){
    if(state.groups === null) state.groups = group
    else state.groups = {...state.groups, ...group}
  },
  removeGroup(state, id){
    delete state.groups[id]
    state.groups = Object.assign({}, state.groups)
  },
  //tasks
  addActiveTask(state, task){
    if(state.activeTasks === null) state.activeTasks = task
    else state.activeTasks = {...state.activeTasks, ...task}
  },
  removeActiveTask(state, id){
    delete state.activeTasks[id]
    state.activeTasks = Object.assign({}, state.activeTasks)
  },
  addArchivedTask(state, task){
    if(state.archivedTasks === null) state.archivedTasks = task
    else state.archivedTasks = {...state.archivedTasks, ...task}
  },
  removeArchivedTask(state, id){
    delete state.archivedTasks[id]
    state.archivedTasks = Object.assign({}, state.archivedTasks)
  },
  //group tasks
  addActiveGroupTask(state, task) {
    if(state.activeGroupTasks === null) state.activeGroupTasks = task
    else state.activeGroupTasks = {...state.activeGroupTasks, ...task}
  },
  removeActiveGroupTask(state, id){
    delete state.activeGroupTasks[id]
    state.activeGroupTasks = Object.assign({}, state.activeGroupTasks)
  },
  addArchivedGroupTask(state, task) {
    if(state.archivedGroupTasks === null) state.archivedGroupTasks = task
    else state.archivedGroupTasks = {...state.archivedGroupTasks, ...task}
  },
  removeArchivedGroupTask(state, id){
    delete state.archivedGroupTasks[id]
    state.archivedGroupTasks = Object.assign({}, state.archivedGroupTasks)
  },
  //group spaces
  addGroupSpace(state, space){
    if(state.groupSpaces === null) state.groupSpaces = space
    else state.groupSpaces = {...state.groupSpaces, ...space}
  },
  removeGroupSpace(state, id){
    delete state.groupSpaces[id]
    state.groupSpaces = Object.assign({}, state.groupSpaces)
  },
  //invitations
  addGroupInvitation(state, invite) {
    if(state.groupInvitations === null) state.groupInvitations = invite
    else state.groupInvitations = {...state.groupInvitations, ...invite}
  },
  removeGroupInvitation(state, id){
    delete state.groupInvitations[id]
    state.groupInvitations = Object.assign({}, state.groupInvitations)
  },
  //setters
  setCurrentGroupId(state, gid){
    state.currentGroupId = gid
  },
  setCurrentSpaceId(state, sid){
    state.currentSpaceId = sid
  },
  setTask(state, task){
    state.task = task
  }
}

export default mutations
