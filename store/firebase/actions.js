import { isEmpty, formattedDate, colors } from '@/assets/utils/utils.js'

async function attachSubcollection({task, id, type, subcollection, firestore, isAuthenticated}) {
  //console.log(id, subcollection)
  let ref = firestore.collection('tasks').doc(id).collection(subcollection).orderBy('timestamp', 'asc')
  if(type == 'group') ref = firestore.collection('groupTasks').doc(id).collection(subcollection).orderBy('timestamp', 'asc')
  let unsubscribe = await ref.onSnapshot(sub => {
    sub.docChanges().forEach(async (change) => {
      let event$ = change.doc.data()
      if(event$.timestamp == null) {
        event$.timestamp = formattedDate(new Date(), true)
      }
      else {
        event$.timestamp = formattedDate(event$.timestamp.toDate(), true)
      }
      Object.defineProperty(event$, 'id', {value: change.doc.id, writeable: false})
      if(change.type == "added" && !isEmpty(task[subcollection])) {
        task[subcollection].unshift(event$)
      }
    })
  }, error => {
    if(!isAuthenticated) unsubscribe()
  })
  return task
}

const actions = {
  //
  //register firebase-database collections to vuex-store Objects
  //
  async setProfilesRef({state, commit, getters}){
    let unsubscribe = await this.$fireStore.collection('profiles').onSnapshot(profiles => {
      /*
        add = loaded for the first time
        modified = added to firestore
        removed = removed
      */
      profiles.docChanges().forEach(async (change) => {
        let profile = change.doc.data()
        Object.defineProperty(profile, 'id', {value: change.doc.id, writeable: false})
        if(change.type == "added" || change.type == "modified") {
          await commit('addProfile', { [change.doc.id]: profile})
        }
        else if(change.type == "removed") {
          await commit('removeProfile', change.doc.id)
        }
      })
    }, error => {
      if(!getters.isAuthenticated) unsubscribe()
    })
  },
  async setProfileRef({state, commit, dispatch}, {uid}) {

  },
  async setGroupsRef({state, commit, dispatch, getters}, uid){
    let unsubscribe = await this.$fireStore.collection('groups').where('members', 'array-contains', uid).onSnapshot(groups => {
      /*
        add = loaded for the first time
        modified = added to firestore
        removed = removed
      */
      groups.docChanges().forEach(async (change) => {
        let group = change.doc.data()
        Object.defineProperty(group, 'id', {value: change.doc.id, writeable: false})
        if(change.type == "added" || change.type == "modified") {
          await commit('addGroup', { [change.doc.id]: group})
        }
        else if(change.type == "removed") {
          await commit('removeGroup', change.doc.id)
        }
        //set group's tasks and spaces only when group is created or when page loads for the first time
        if(change.type == "added") {
          dispatch('setActiveGroupTasksRef', change.doc.id)
          dispatch('setGroupSpacesRef', change.doc.id)
        }
      })
    }, error => {
      if(!getters.isAuthenticated) unsubscribe()
    })
  },
  async setSpacesRef({state, commit, getters}, uid){
    let unsubscribe = await this.$fireStore.collection('spaces').where('uid', '==', uid).onSnapshot(spaces => {
      /*
        add = loaded for the first time
        modified = added to firestore
        removed = removed
      */
      spaces.docChanges().forEach(async (change) => {
        let space = change.doc.data()
        Object.defineProperty(space, 'id', {value: change.doc.id, writeable: false})
        if(change.type == "added" || change.type == "modified") {
          await commit('addSpace', { [change.doc.id]: space})
        }
        else if(change.type == "removed") {
          await commit('removeSpace', change.doc.id)
        }
      })
    }, error => {
      if(!getters.isAuthenticated) unsubscribe()
    })
  },
  async setGroupInvitationsRef({state, commit, getters}, uid){
    let unsubscribe = await this.$fireStore.collection('groupInvitations').where('uid', '==', uid).onSnapshot(invites => {
      /*
        add = loaded for the first time
        modified = added to firestore
        removed = removed
      */
      invites.docChanges().forEach(async (change) => {
        let invite = change.doc.data()
        Object.defineProperty(invite, 'id', {value: change.doc.id, writeable: false})

        if(change.type == "added" || change.type == "modified") {
          await commit('addGroupInvitation', { [change.doc.id]: invite})
        }
        else if(change.type == "removed") {
          await commit('removeGroupInvitation', change.doc.id)
        }
      })
    }, error => {
      if(!getters.isAuthenticated) unsubscribe()
    })
  },
  async setActiveTasksRef({state, commit, getters}, uid){
    let unsubscribe = await this.$fireStore.collection('tasks').where('uid', '==', uid).where('status', '==', 0).onSnapshot(tasks => {
      /*
        add = loaded for the first time
        modified = added to firestore
        removed = removed
      */
      tasks.docChanges().forEach(async (change) => {
        let task = change.doc.data()
        Object.defineProperty(task, 'id', {value: change.doc.id, writeable: false})
        task.history = []
        task.comments = []
        if((change.type == "added" || change.type == "modified") && task.timestamp_start !== null) {
          task.new = false
          let date = task.timestamp_start.toDate()
          if(Math.floor(new Date() - date) / 36e5 < 8){
            task.new = true
          }
          let t = await attachSubcollection({
            task: task,
            id: change.doc.id,
            type: 'private',
            subcollection: 'history',
            firestore: this.$fireStore,
            isAuthenticated: getters.isAuthenticated
          })
          t = await attachSubcollection({
            task: t,
            id: t.id,
            type: 'private',
            subcollection: 'comments',
            firestore: this.$fireStore,
            isAuthenticated: getters.isAuthenticated
          })
          await commit('addActiveTask', { [change.doc.id]: t })
        }
        else if(change.type == "removed") {
          await commit('removeActiveTask', change.doc.id)
        }
      })
    }, error => {
      if(!getters.isAuthenticated) unsubscribe()
    })
  },
  async setArchivedTasksRef({state, commit, getters}, uid){
    let unsubscribe = await this.$fireStore.collection('tasks').where('uid', '==', uid).where('status', '==', 1).onSnapshot(tasks => {
      /*
        add = loaded for the first time
        modified = added to firestore
        removed = removed
      */
      tasks.docChanges().forEach(async (change) => {
        let task = change.doc.data()
        Object.defineProperty(task, 'id', {value: change.doc.id, writeable: false})
        task.history = []
        task.comments = []
        if((change.type == "added" || change.type == "modified") && task.timestamp_start !== null) {
          task.new = false
          let date = task.timestamp_start.toDate()
          if(Math.floor(new Date() - date) / 36e5 < 8){
            task.new = true
          }
          let t = await attachSubcollection({
            task: task,
            id: change.doc.id,
            type: 'private',
            subcollection: 'history',
            firestore: this.$fireStore,
            isAuthenticated: getters.isAuthenticated
          })
          t = await attachSubcollection({
            task: t,
            id: t.id,
            type: 'private',
            subcollection: 'comments',
            firestore: this.$fireStore,
            isAuthenticated: getters.isAuthenticated
          })
          await commit('addArchivedTask', { [change.doc.id]: t })
        }
        else if(change.type == "removed") {
          await commit('removeArchivedTask', change.doc.id)
        }
      })
    }, error => {
      if(!getters.isAuthenticated) unsubscribe()
    })
  },
  //initially
  async setActiveGroupTasksRef({state, commit, getters}, gid){
    let unsubscribe = await this.$fireStore.collection('groupTasks').where('gid', '==', gid).where('status', '==', 0).onSnapshot(tasks => {
      /*
        add = loaded for the first time
        modified = added to firestore
        removed = removed
      */
      tasks.docChanges().forEach(async (change) => {
        let task = change.doc.data()
        Object.defineProperty(task, 'id', {value: change.doc.id, writeable: false})
        task.history = []
        task.comments = []

        if((change.type == "added" || change.type == "modified") && task.timestamp_start !== null) {
          task.new = false
          let date = task.timestamp_start.toDate()
          if(Math.floor(new Date() - date) / 36e5 < 8){
            task.new = true
          }
          let t = await attachSubcollection({
            task: task,
            id: change.doc.id,
            type: 'group',
            subcollection: 'history',
            firestore: this.$fireStore,
            isAuthenticated: getters.isAuthenticated
          })
          t = await attachSubcollection({
            task: t,
            id: t.id,
            type: 'group',
            subcollection: 'comments',
            firestore: this.$fireStore,
            isAuthenticated: getters.isAuthenticated
          })
          await commit('addActiveGroupTask', { [change.doc.id]: await t})
        }
        else if(change.type == "removed") {
          await commit('removeActiveGroupTask', change.doc.id)
        }
      })
    }, error => {
      if(!getters.isAuthenticated) unsubscribe()
    })
  },
  async setArchivedGroupTasksRef({state, commit, getters}, gid){
    let unsubscribe = await this.$fireStore.collection('groupTasks').where('gid', '==', gid).where('status', '==', 1).onSnapshot(tasks => {
      /*
        add = loaded for the first time
        modified = added to firestore
        removed = removed
      */
      tasks.docChanges().forEach(async (change) => {
        let task = change.doc.data()
        Object.defineProperty(task, 'id', {value: change.doc.id, writeable: false})
        task.history = []
        task.comments = []

        if((change.type == "added" || change.type == "modified") && task.timestamp_start !== null) {
          task.new = false
          let date = task.timestamp_start.toDate()
          if(Math.floor(new Date() - date) / 36e5 < 8){
            task.new = true
          }
          let t = await attachSubcollection({
            task: task,
            id: change.doc.id,
            type: 'group',
            subcollection: 'history',
            firestore: this.$fireStore,
            isAuthenticated: getters.isAuthenticated
          })
          t = await attachSubcollection({
            task: t,
            id: t.id,
            type: 'group',
            subcollection: 'comments',
            firestore: this.$fireStore,
            isAuthenticated: getters.isAuthenticated
          })
          await commit('addArchivedGroupTask', { [change.doc.id]: t})
        }
        else if(change.type == "removed") {
          await commit('removeArchivedGroupTask', change.doc.id)
        }
      })
    }, error => {
      if(!getters.isAuthenticated) unsubscribe()
    })
  },
  async getTaskById({state, commit, getters}, {uid, tid}) {
      let task = await this.$fireStore.collection('groupTasks').doc(tid).get()
      if(!isEmpty(task.data())) {
        let t = await attachSubcollection({
          task: task.data(),
          id: task.id,
          type: 'group',
          subcollection: 'history',
          firestore: this.$fireStore,
          isAuthenticated: getters.isAuthenticated
        })

        t = await attachSubcollection({
          task: t,
          id: task.id,
          type: 'group',
          subcollection: 'comments',
          firestore: this.$fireStore,
          isAuthenticated: getters.isAuthenticated
        })
        t.history = []
        t.comments = []
        commit('setTask', t)
      }
      else {
        //try {
          //
          console.log('here', tid, uid)
          task = await this.$fireStore.collection('tasks').where(this.$fireStoreObj.FieldPath.documentId(), '==', tid).where('uid', '==', uid).get()
          if(!isEmpty(task.docs[0].data())) {
            let t = await attachSubcollection({
              task: task.docs[0].data(),
              id: task.docs[0].id,
              type: 'private',
              subcollection: 'history',
              firestore: this.$fireStore,
              isAuthenticated: getters.isAuthenticated
            })
            t = await attachSubcollection({
              task: t,
              id: t.id,
              type: 'private',
              subcollection: 'comments',
              firestore: this.$fireStore,
              isAuthenticated: getters.isAuthenticated
            })
            t.history = []
            t.comments = []
            commit('setTask', t)
          }
        //}
        //catch{ console.log('error') }
      }
  },
  async setGroupSpacesRef({state, commit, getters}, gid){
    let unsubscribe = await this.$fireStore.collection('groupSpaces').where('gid', '==', gid).onSnapshot(spaces => {
      /*
        add = loaded for the first time
        modified = added to firestore
        removed = removed
      */
      spaces.docChanges().forEach(async (change) => {
        let space = change.doc.data()
        Object.defineProperty(space, 'id', {value: change.doc.id, writeable: false})

        if(change.type == "added" || change.type == "modified") {
          await commit('addGroupSpace', { [change.doc.id]: space})
        }
        else if(change.type == "removed") {
          await commit('removeGroupSpace', change.doc.id)
        }
      })
    }, error => {
      if(!getters.isAuthenticated) unsubscribe()
    })
  },
  //after a new group space is added, add a new listener
  async setGroupSpaceRef({state, commit, getters}, gid){
    let unsubscribe = await this.$fireStore.collection('groupSpaces').where('gid', '==', gid).onSnapshot(spaces => {
      /*
        add = loaded for the first time
        modified = added to firestore
        removed = removed
      */
      spaces.docChanges().forEach(async (change) => {
        let space = change.doc.data()
        Object.defineProperty(space, 'id', {value: change.doc.id, writeable: false})

        if(change.type == "added" || change.type == "modified") {
          await commit('addGroupSpace', { [change.doc.id]: space})
        }
        else if(change.type == "removed") {
          await commit('removeGroupSpace', change.doc.id)
        }
      })
    }, error => {
      if(!getters.isAuthenticated) unsubscribe()
    })
  },
  //
  // firebase-storage: upload/delete file/folder
  //
  async uploadFile({state, dispatch}, {path, name, file}){
    //file: path/name = folder/task.id/user.uid
    //where folder = 'tasks' or 'groupTasks'
    return await this.$fireStorage.ref().child(`${path}/${name}`).put(file)
      .catch(error => {
        console.log("error: ", error)
      })
      // get download link right after the upload
      .then(snapshot => {
        snapshot.ref.getDownloadURL().then(async (url) => {
          await dispatch('updateTaskField', {
            field: 'file',
            value: url,
            id: path.split('/')[1], // path = folder/task.id
            message: 'File Uploaded'
          })
        })
      })
  },
  deleteFile({state}, {path, name}){
    if(isEmpty(name) || name === '') return
    try {
      this.$fireStorage.ref().child(`${path}/${name}`).delete()
    }
    catch(error){
      console.log(error)
    }
  },
  deleteStorageFolder({state, dispatch}, {path}){
    const ref = this.$fireStorage.ref(path)
    ref.listAll()
      .then(dir => {
        dir.items.forEach(fileRef => {
          dispatch('deleteFile', {path: ref.fullPath, name: fileRef.name})
        })
        dir.prefixes.forEach(folderRef => {
          dispatch('deleteStorageFolder', {path: folderRef.fullPath})
        })
      })
      .catch(error => {
        console.log(error);
      })
  },
  //
  //user related firebase functions (read-only)
  //
  async userAuthChange({state, dispatch, commit}, {authUser, claims}) {
    if (authUser) {
      try {
        commit('setUser', authUser)
        await dispatch('setProfilesRef', authUser.id)
        await dispatch('setGroupsRef', authUser.uid)
        await dispatch('setSpacesRef', authUser.uid)
        await dispatch('setActiveTasksRef', authUser.uid)
        await dispatch('setGroupInvitationsRef', authUser.uid)
      } catch (e) {
        console.error(e)
      }
    }
    else{
      //dispatch('userLogout')
    }
  },
  async userSignUp({commit, dispatch}, {email, password, name}) {
    try {
      let newUser = await this.$fireAuth.createUserWithEmailAndPassword(email, password)
      await dispatch('userCreateNewProfile', {user:newUser.user, name})
      await dispatch('userLogin', {email, password})
    }
    catch (e) {
      alert(e)
    }
  },
  async userCreateNewProfile({dispatch}, {user, name}) {
    let profile = {
      name: name,
      defaultGroup: 'Private',
      defaultSpace: '',
      email: user.email,
      groups: [],
      spaces: []
    }
    // create user profile reference
    await this.$fireStore.collection('profiles').doc(user.uid).set(profile)
  },
  async userLogin({state, commit, dispatch}, user)  {
    let u = await this.$fireAuth.signInWithEmailAndPassword(user.email, user.password)
    await dispatch('userAuthChange', {authUser : u.user})
  },
  userLogout ({state}) {
    return this.$fireAuth.signOut().then(() => {
      this.dispatch('firebase/userReset')
    })
  },
  resetPassword({state}, email) {
    this.$fireAuth.sendPasswordResetEmail(email)
  },
  userReset ({commit}) {
    commit('userReset')
  },
  resetPassword({state}, email){
    return this.$fireAuth.sendPasswordResetEmail(email)
  },
  //
  //database manipulation
  //
  // Additions
  //
  async addNewTask({state, dispatch, getters}, task){
    let upload = task.file !== '' ? true : false
    let obj = {}

    if(upload){
      obj = {
        path: 'tasks',
        name: state.user.uid,
        file: task.file
      }
      task.file = "."
    }
    task.status = 0
    task.timestamp_start = this.$fireStoreObj.FieldValue.serverTimestamp()

    let ref = this.$fireStore.collection('tasks')
    let history = {
      timestamp: this.$fireStoreObj.FieldValue.serverTimestamp(),
      message: 'Task Created',
      uid: state.user.uid
    }
    if(getters.group) {
      ref = this.$fireStore.collection('groupTasks')
      history.gid = getters.group.id
      task.gid = getters.group.id
      obj.path = 'groupTasks'
    }

    await ref.add(task).then(async (docRef) => {
      ref.doc(docRef.id).collection('history').add(history)
      if(upload) {
        obj.path = `${obj.path}/${docRef.id}`
        dispatch('uploadFile', obj)
      }
    })
  },
  async saveTaskComment({state, commit, getters}, {tid, text}) {
    let ref = this.$fireStore.collection('tasks')
    if(getters.group) {
      ref = this.$fireStore.collection('groupTasks')
    }
    let comment = {text: text}
    comment.timestamp = this.$fireStoreObj.FieldValue.serverTimestamp()
    comment.uid = state.user.uid
    ref.doc(tid).collection('comments').add(comment).then(docRef => {
      console.log(`/${tid}/comments/${docRef.id}`)
    })
  },
  async addNewGroup({state, dispatch}, group){
    // 1. add new group to groups collection
    // 2. send group invites to new members
    // 3. add group id to user's profile
    let gid = ''
    await this.$fireStore.collection('groups').add(group).then(docRef => {
      gid = docRef.id
      if(group.invitie.length > 0) {
        dispatch('sendGroupInvitations', {gid: gid, members: group.invitie})
      }
      dispatch('setCurrentGroupId', gid)
    })
    let uid = group.members[0]
    await dispatch('updateProfile', {
      id: uid,
      field: 'groups',
      value: this.$fireStoreObj.FieldValue.arrayUnion(gid)
    })
  },
  async addNewSpace({state, dispatch, getters}, space){
    // 1. add and color to space
    // 2. add new space document (normal or group)
    // 3.a. if group space: add space id to group document
    // 3.b. else add space id to profile document
    // 4. set current space as active space

    //start index from 1. index 0 will be the default space when set.
    if(getters.activeSpaces) {
      space.color = colors[Object.keys(getters.activeSpaces).length]
    }
    else {
      space.color = colors[0]
    }

    if(getters.group) {
      space.gid = getters.group.id
      await this.$fireStore.collection('groupSpaces').add(space).then(docRef => {
        if(getters.group.defaultGroupSpace == '' || getters.group.defaultGroupSpace == null) {
          this.$fireStore.collection('groups').doc(getters.group.id).update({defaultGroupSpace: docRef.id})
        }
        dispatch('updateGroup', {
          id: getters.group.id,
          field: 'spaces',
          value: this.$fireStoreObj.FieldValue.arrayUnion(docRef.id)
        })
        dispatch('setCurrentSpaceId', docRef.id)
      })
      //dispatch('setCurrentSpace', state.groupSpaces[docRef.id])
    }
    else {
      await this.$fireStore.collection('spaces').add(space).then(docRef => {
        if(getters.profile.defaultSpace == '' || getters.profile.defaultSpace == null) {
          this.$fireStore.collection('profiles').doc(state.user.uid).update({defaultSpace: docRef.id})
        }
        dispatch('updateProfile', {
          id: getters.profile.id,
          field: 'spaces',
          value: this.$fireStoreObj.FieldValue.arrayUnion(docRef.id)
        })
        dispatch('setCurrentSpaceId', docRef.id)
      })
    }
  },
  async sendGroupInvitations({state, dispatch}, {gid, members}){
    members.forEach(member => {
      this.$fireStore.collection('groupInvitations').add({
        uid: member,
        gid: gid,
        sid: state.user.uid,
        groupName: state.groups[gid].name
      })
      this.$fireStore.collection('groups').doc(gid).update({invitie: this.$fireStoreObj.FieldValue.arrayUnion(member)})
    })
  },
  // Updates
  //
  updateProfile({state}, {field, value, id}){
    return this.$fireStore.collection('profiles').doc(id).update({[field]: value})
  },
  async updateTask({state, commit, getters}, {task, message}){
    let ref = this.$fireStore.collection('tasks')
    let history = {
      timestamp: this.$fireStoreObj.FieldValue.serverTimestamp(),
      message: message,
      uid: state.user.uid
    }
    if(getters.group) {
      ref = this.$fireStore.collection('groupTasks')
      history.gid = getters.group.id
    }
    delete task.history
    delete task.comments

    await ref.doc(task.id).update(task)
    await ref.doc(task.id).collection('history').add(history)
  },
  async updateTaskField({state, commit, getters}, {field, value, id, message}){
    let ref = this.$fireStore.collection('tasks')
    let history = {
      timestamp: this.$fireStoreObj.FieldValue.serverTimestamp(),
      message: message,
      uid: state.user.uid
    }
    if(getters.group) {
      ref = this.$fireStore.collection('groupTasks')
      history.gid = getters.group.id
    }
    //update task field
    await ref.doc(id).update({[field]: value})
    //update end date
    if(field === 'status' && value === 1) {
      await ref.doc(id).update({'timestamp_end': history.timestamp})
    }
    if(field === 'status' && value === 0) {
      await ref.doc(id).update({'timestamp_end': null})
    }
    await ref.doc(id).collection('history').add(history)
  },
  updateSpace({state, getters}, {id, field, value}){
    let ref = this.$fireStore.collection('spaces')
    if(getters.group)
      ref = this.$fireStore.collection('groupSpaces')
    ref.doc(id).update({[field] : value})
  },
  updateGroup({state}, {id, field, value}){
    this.$fireStore.collection('groups').doc(id).update({[field] : value})
  },
  async acceptInvite({state, dispatch}, {gid, iid, uid}){
    // 1. update group (gid)
    // 2. update the new member profile (uid)
    // 3. delete invite (iid) (subsecuentiually remove uid from group.invitie array)
    await dispatch('updateGroup', {
      id: gid,
      field: 'members',
      value: this.$fireStoreObj.FieldValue.arrayUnion(uid)
    })
    await dispatch('updateGroup', {
      id: gid,
      field: 'invitie',
      value: this.$fireStoreObj.FieldValue.arrayRemove(uid)
    })
    await dispatch('updateProfile', {
      id: uid,
      field: 'groups',
      value: this.$fireStoreObj.FieldValue.arrayUnion(gid)
    })
    await dispatch('deleteInvite', {id: iid, uid: uid, gid: gid})
  },
  async deleteInvite({state, dispatch}, {id, uid, gid}){
    // 1. Delete invite
    await this.$fireStore.collection('groupInvitations').doc(id).delete()
    // 2. remove uid from group.invitie array
    await dispatch('updateGroup', {
      id: gid,
      field: 'invitie',
      value: this.$fireStoreObj.FieldValue.arrayRemove(uid)
    })
  },
  //
  // deletions/removals
  //
  // cloud functions
  //
  deleteTask({state, dispatch, getters}, {task}) {
    let path = 'tasks'
    if(getters.group) {
      path = 'groupTasks'
    }
    let v = this.$fireFunc.httpsCallable('recursiveDelete')
    v(({path: `/${path}/${task.id}`, uid: state.user.uid}))
    .catch(err => console.log('Delete failed: ', err))
    if(task.file) {
      dispatch('deleteFile', {
        path: `/${path}/${task.id}`,
        name: state.user.uid
      })
    }
  },
  async deleteSpace({state, dispatch, getters}, id){
    // 1. delete space document
    let ref = this.$fireStore.collection('spaces')
    let tasks = state.tasks

    //path and object to update profile/group defaultspace enrty (3.)
    let path = 'updateProfile'
    let defaultSpace = {
      id: getters.profile.id,
      field: 'defaultSpace',
      value: ''
    }
    let space = {
      id: getters.profile.id,
      field: 'spaces',
      value: this.$fireStoreObj.FieldValue.arrayRemove(id)
    }

    if(getters.group) {
      ref = this.$fireStore.collection('groupSpaces')
      tasks = state.groupTasks
      path = 'updateGroup'
      defaultSpace = {
        id: getters.group.id,
        field: 'defaultGroupSpace',
        value: ''
      }
      space = {
        id: getters.group.id,
        field: 'spaces',
        value: this.$fireStoreObj.FieldValue.arrayRemove(id)
      }
    }
    await ref.doc(id).delete()
    // 2. delete all tasks documents belonging to that space (including their history subcollections)
    if(!isEmpty(tasks)) {
      tasks = tasks.filter(task => task.space == id)
      tasks.forEach(async (task) => {
        await dispatch('deleteTask', {task: task})
      })
    }
    // 3. remove space from profile/group document
    dispatch(path, space)
    // 4. if it was the default space remove it from profile/group document
    if(getters.defaultSpace == id){
      dispatch(path, defaultSpace)
    }
    // 5. set current space the next space in the object else null
    if(!isEmpty(getters.allSpaces)) {
      dispatch('setCurrentSpaceId', Object.keys(getters.allSpaces)[0].id)
    }
    else {
      dispatch('setCurrentSpaceId', null)
    }
  },
  async leaveGroup({state, dispatch}, {gid, uid}){
    // 1. remove gid from user's profile
    await dispatch('updateProfile', {
      id: uid,
      field: 'groups',
      value: this.$fireStoreObj.FieldValue.arrayRemove(gid)
    })
    // 2. remove uid from group's members
    await dispatch('updateGroup', {
      id: gid,
      field: 'members',
      value: this.$fireStoreObj.FieldValue.arrayRemove(uid)
    })
  },
  async removeGroupMember({state, dispatch}, {gid, mid, val}){
    await this.$fireStore.collection('groups').doc(gid).update({
      members: this.$fireStoreObj.FieldValue.arrayRemove(mid)
    })
    await dispatch('updateProfile', {
      id: mid,
      field: 'groups',
      value: this.$fireStoreObj.FieldValue.arrayRemove(gid)
    })
  },
  //
  // mutations setters
  //
  setCurrentSpaceId({state, commit}, sid){
    commit('setCurrentSpaceId', sid)
  },
  setCurrentGroupId({state, commit}, gid){
    commit('setCurrentGroupId', gid)
  }
}

export default actions
