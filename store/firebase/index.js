import Vuex from 'vuex'

import state from '@/store/firebase/state.js'
import actions from '@/store/firebase/actions.js'
import getters from '@/store/firebase/getters.js'
import mutations from '@/store/firebase/mutations.js'

export default {
  namespaced: true,
  mutations, actions, getters, state
}
