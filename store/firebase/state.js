const state = () => ({
  //user
  user: null,
  profiles: null,
  //tasks
  activeTasks: null,
  archivedTasks: null,
  task: null,
  //groups
  groups: null,
  groupSpaces: null,
  activeGroupTasks: null,
  archivedGroupTasks: null,
  currentGroupId: null,
  //spaces
  currentSpaceId: null,
  spaces: null,
  //Invites
  groupInvitations: null
})

const initialState = state

export default state
