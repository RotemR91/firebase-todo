import firebase from 'firebase/app'
import 'firebase/auth'

export default ({store, redirect}) => {
  if(process.client && store.$fireAuth.currentUser) {
    redirect('/')
  }
}
