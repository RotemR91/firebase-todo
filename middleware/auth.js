import firebase from 'firebase/app'
import 'firebase/auth'

export default ({redirect, store}) => {
  if(process.client && !store.$fireAuth.currentUser){
      redirect('/user/signin')
  }
}
